# virtual_paint

Virtual_paint with python and OpenCV.
## Structure
 1. Analiza - cantine analiza gesturilor unui grup de peroane.
 2. VirtualPaint - Aplicatie in python cu OpenCv care permite desenarea cu ajutorul camerei utilizand gesturile maini.
  
## Getting started
VirtualPaint  contine 3 fisiere principale:
 1. VolumePicker - permite cresterea sau micsorarea volumului utilizand gesturile mainii
 2. VirtualPainter - Permite desenarea cu ajutorul camerei de filmat ptin intermediul gesturilor mainii.
 3. HandTrackingModule - Modul pentru recunoasterea mainii si a pozitiilor degetelor.
