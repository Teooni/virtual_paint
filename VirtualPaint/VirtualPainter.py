import cv2
import numpy as np
import time
import os
import HandTrackingModule as htm
import math
from datetime import datetime

#######################
minSize = 3
maxSize = 200
brushThickness = 15
eraserThickness = 150
wCam, hCam = 1280, 720
resize = False
eraserSelected = False
xp, yp = 0, 0
pTime = 0

# used to save the final image painted in img dir
directory = f'{os.path.dirname(__file__)}/img'
# print(directory)
# current date and time
#now = datetime.now()
# ts stores the time in seconds
ts = time.time()
# Filename
filename = f'img_{ts}.jpg'
#######################

folderPath = "Background"
myList = os.listdir(folderPath)
print(myList)
# contains all posible backgrounds
overlayList = []
for imPath in myList:
    image = cv2.imread(f'{folderPath}/{imPath}')
    overlayList.append(image)
print(len(overlayList))

header = overlayList[0]
drowClor = (255, 255, 255)
cap = cv2.VideoCapture(0)
cap.set(3, wCam)
cap.set(4, hCam)

detector = htm.handDetector(detectionCon=0.85)

imgCanvas = np.zeros((720, 1280,3), np.uint8 )

while True:
    # 1. import Image
    success, img = cap.read()
    img = cv2.flip(img, 1) # mirrow mode
    # 2. Find Hand Landmarks
    img = detector.findHands(img)
    lmList = detector.findPosition(img, draw=False)
    # If we detected position for hand
    if len(lmList) != 0:
        #print(lmList)
        # type of index and middle finger
        x1, y1 = lmList[8][1:]
        x2, y2 = lmList[12][1:]

        # 3. Check witch fingers are up
        fingers = detector.fingersUP()
        print(fingers)

        # 4. If selection mode - Two fingers are up
        if fingers[1] and fingers[2]:
            xp, yp = 0, 0
            print("Selection mode")
            resize = False
            # Checking clicked
            if y1 < 125:
                if 0 < x1 < 250:
                    resize = True
                if 250< x1 < 450:
                    header = overlayList[0]
                    drowClor = (255, 255, 255)
                    resize = False
                    eraserSelected = False
                elif 550 < x1 < 750:
                    header = overlayList[1]
                    drowClor = (255, 0, 0)
                    resize = False
                    eraserSelected = False
                elif 800 < x1 < 950:
                    header = overlayList[2]
                    drowClor = (0, 255, 0)
                    resize = False
                    eraserSelected = False
                if 1050 < x1 < 1200:
                    header = overlayList[3]
                    drowClor = (0, 0, 0)
                    resize = False
                    eraserSelected = True

            cv2.rectangle(img, (x1 - 5, y1 - 25), (x2 + 5, y2 + 25), drowClor, cv2.FILLED)
        # 5.  If Drawing Mode - Index finger is up
        if fingers[1] and fingers[2] == False and resize == False:
            cv2.circle(img, (x1,y1),15,drowClor,cv2.FILLED)
            print("Drawing mode")
            if xp ==0 and yp == 0:
                xp, yp = x1,y1

            if drowClor == (0, 0, 0):
                print(xp, yp, x1, y1)
                cv2.line(img, (xp, yp), (x1, y1), drowClor, int(eraserThickness))
                cv2.line(imgCanvas, (xp, yp), (x1, y1), drowClor, int(eraserThickness))
            else:
                print(xp, yp, x1, y1)
                cv2.line(img, (xp, yp), (x1, y1), drowClor, int(brushThickness))
                cv2.line(imgCanvas, (xp, yp), (x1, y1), drowClor, int(brushThickness))

            xp, yp = x1, y1
        # 6.  If Resize mode - Index finger is up
        if resize:
            xr1, yr1 = lmList[4][1], lmList[4][2]
            xr2, yr2 = lmList[8][1], lmList[8][2]
            crx, cry = (xr1 + xr2) // 2, (yr1 + yr2) // 2

            cv2.circle(img, (xr1, yr1), 15, (255, 0, 255), cv2.FILLED)
            cv2.circle(img, (xr2, yr2), 15, (255, 0, 255), cv2.FILLED)
            cv2.line(img, (xr1, yr1), (xr2, yr2), (255, 0, 255), 3)
            cv2.circle(img, (crx, cry), 15, (255, 0, 255), cv2.FILLED)

            length = math.hypot(xr2 - xr1, yr2 - yr1)

            if length < 50:
                cv2.circle(img, (crx, cry), 15, (0, 255, 0), cv2.FILLED)

            if eraserSelected:
                eraserSize = np.interp(length, [50, 300], [minSize, maxSize])
                eraserThickness = eraserSize
            else:
                brushSize = np.interp(length, [50, 300], [minSize, maxSize])
                brushThickness = brushSize


    imgGray = cv2.cvtColor(imgCanvas, cv2.COLOR_BGR2GRAY)
    _, imgInv = cv2.threshold(imgGray, 50, 255, cv2.THRESH_BINARY_INV)
    imgInv = cv2.cvtColor(imgInv, cv2.COLOR_GRAY2BGR)
    img = cv2.bitwise_and(img, imgInv)
    img = cv2.bitwise_or(img, imgCanvas)

    cTime = time.time()
    fps = 1 / (cTime - pTime)
    pTime = cTime
    cv2.putText(img, f'FPS:{int(fps)}', (0, 700), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 0), 3)

    cv2.putText(img, f'| Brush Size:{int(brushThickness)} | Eraser Size:{int(eraserThickness)}', (139, 700), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 0), 3)
    # Setting the hedder
    img[0:125, 0:1280] = header
    # img = cv2.addWeighted(img,0.5,imgCanvas,0.5,0)
    cv2.imshow("Image", img)
    #cv2.imshow("Canvas", imgCanvas)
    #cv2.imshow("IMG inverse", imgInv)
    if cv2.waitKey(1) == ord('q'):
        # Change the current directory
        # to specified directory
        os.chdir(directory)

        # List files and directories
        print("Before saving image:")
        print(os.listdir(directory))

        # Using cv2.imwrite() method
        # Saving the image
        print(filename)
        cv2.imwrite(filename, img)

        # List files and directories
        print("After saving image:")
        print(os.listdir(directory))
        #time.sleep(5)
        break

cap.release()
cv2.destroyAllWindows()